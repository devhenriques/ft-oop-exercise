Object Oriented Programming Exercise
====================================
Use object oriented design pattern with TDD to implement classes and methods with appropriate data structures.

Assumptions
-----------
* There are Teachers
* There are Students
* Students are in classes that teachers teach
* Teachers can assign quizzes to students
* Students solve/answer questions to complete the quiz, but they don't have to complete it at once. (Partial submissions can be made)
* Quizzes need to get graded
* For each teacher, they can calculate each student's total grade accumulated over a semester for their classes
